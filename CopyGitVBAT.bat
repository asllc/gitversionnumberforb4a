@cls
@REM @if [%1%] == [] goto noarg
@echo.

@pushd
@set P1= "c:\Program Files (x86)\Anywhere Software"
@set P2=AdditionalLibraries
set P2
@if [%1%] == [] goto noarg
cd %P1%\%1\%P2%
@goto cd2 
:noarg
cd %P1%\%P2%
@if ERRORLEVEL = 1 goto badfolder
:cd2
@popd


@if [%1%] == [] goto noarg2
copy gitv.bat %P1%\%1\%P2%\*.*
goto copy2
:noarg2
copy gitv.bat %P1%\%P2%\*.*
:copy2
@if ERRORLEVEL = 1 goto badcopy
REM @cls
@echo.
@echo Copy operation successfully completed.
@echo.
@echo.
@goto ok
:badfolder
@echo.
@echo NO SUCH Folder exists
@echo.
@goto ok
:badcopy
@echo.
@echo The copy operation failed.
@echo.
@goto ok
:noargx
@echo.
@echo No path argument entered.
@echo.
@goto ok

:ok
@set P1=
@set P2=
