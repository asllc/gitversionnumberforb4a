
@cls

@REM '!!! This batch file generates an xml file with an EMPTY gitversion_num String which you 
@REM '!!! is needed to get the B4A git version system going.
@REM '!! it makes the FOLDER, then creates a dummy xml file, then makes the file READ ONLY.

@cd Objects
@if ERRORLEVEL = 1 Goto BadFolder
@cd ..
REM @Goto OK
@mkdir Objects\res\values

@echo ^<?xml version="1.0" encoding="utf-8"?^>^<resources^>  > Objects\res\values\gitversion.xml

@echo    ^<string name="gitversion_num"^>   >> Objects\res\values\gitversion.xml

@echo ^<^/string^>  >> Objects\res\values\gitversion.xml

@echo ^<^/resources^>  >> Objects\res\values\gitversion.xml

@c:\windows\system32\attrib.exe +r Objects\res\values\gitversion.xml

@REM !!! It may be possible to trap the stderr, but this has not been done yet.
@Goto OK

:BadFolder

@Echo This batch file must be executed from the folder that contains the B4A "Objects" folder.
@Echo.

:OK

@cd Objects\res\values
@if ERRORLEVEL = 1 Goto NoFolder
@cd ..\..\..\
@Goto OK2

:NoFolder
@Echo This batch file did not create the B4A "Objects\res\values" folder.
@Echo.
@exit

:OK2
@cd
@Echo This batch file succesfully created the B4A "Objects\res\values" folder and gitversion.xml file.
@Echo.
