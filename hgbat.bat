SET HGCMD=

REM If NOT Exist .hg GOTO NOHG


SET HGPATH=%ProgramFiles%\TortoiseHg\hg.exe
Echo %HGPATH%
If Exist "%HGPATH%" GOTO GOTHG
SET HGPATH=%ProgramFiles(x86)%\TortoiseHg\hg.exe
Echo %HGPATH%
If Exist %%HGPATH%% GOTO GOTHG
SET HGPATH=""
SET HGCMD=""
GOTO NOHG
:GOTHG
SET HGCMD="%HGPATH%" parents --template '{latesttag}+{latesttagdistance}'
:NOHG

dir "%HGPATH%" 1>NUL 2> NUL
if %ERRORLEVEL% EQU 0 (
    echo is a file
) else (
    echo is NOT a file
)
Echo %HGCMD%

%HGCMD%

SET GITPATH="%ProgramFiles(x86)%"\Git\bin\git.exe
%GITPATH% describe --tags --long --always	:: not git repository returns 128
echo %ERRORLEVEL%
If %ERRORLEVEL% == 255 Echo NO RESPOSITORY XXX
