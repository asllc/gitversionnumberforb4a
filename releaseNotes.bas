﻿Type=StaticCode
Version=3
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub
' Tested with Windows 7
' The XML file contains extra data not used by the buildnumber number. This does not cause
' any problem, except that the B4A code has to strip out the build number.

' The batch file does not contain certain batch features that may not be portable.
' In other words, I used only constructs that I was certain would work most anywhere(Windows)

' The CustomBuild line in B4A has a bug, in that if you don't specify a specific path
' to the batch file you will get a PATH error message. In other words it would seem that if the 
' batch file was in your library path it would be found, or if in your project root directory.
' But, it will not be found and until this is fixed the batch has to be somewhere, I chose the 
' extra library path.

' I suggest that you place your addtional library in the root of AnywhereSoftware.
' Not in one of the B4A version folders.

' The buildnum.exe file is duplicated in each project. You may argue that it should be 
' placed in one folder only, but since this entire git version/buildnumber solution is a 
' first clase KLUDGE and since the exe file might not be included with the project later, 
' if not included always, I chose the project folder. And duplication be hanged.

' If you want to disable the git version functionality, add "NOGIT", without the quotes 
' as the dummy_arg1.







