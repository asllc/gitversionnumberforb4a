﻿Type=StaticCode
Version=3
@EndOfDesignText@

'Provided free of charge, without warranty by Mike Maurice, mikem at yachtsdelivered.com
'http://en.wikibooks.org/wiki/Windows_Batch_Scripting

'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! G I T Version Comments  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
' Git Version Number: Tag, Commit number since last tag, and Hash (Hash in hex preceded by a "g", all lower case hex)
' Similar to this: V1Bat-1-ga62fb1
' If no tag created yet, returns the last hash.

' 1. Place the sCodeVerNum.bat file. This is a one time task.
' NOTICE where the sCodeVerNum.bat file must be placed. It can be used by any program. This path is part of the tools configure path list.
' Be sure to place it in the same place where your additional libraries path is.
' You can get the path from Tools, Configure Paths, Additional Libraries, substitute it below
'#CustomBuildAction: 1, C:\Program Files (x86)\Anywhere Software\AdditionalLibraries\sCodeVerNum.bat, dummy_arg

' Instead of steps 2-6 below, you can use TestingGitV as a template to start ANY b4a project.
' just rename the Project/PackageName and the #Application Label(above) and the core TestingGitV disk file.

' 2. Put the Buildnum.exe file in your project root.
' 3. ADD this Custom Build Action line, directly above, into your project attributes, like here.
' 4. Then add the Private strings in Process_Globals
' 5. Then add the lines from Activity_Create, and the sCodeVerNum code module.
' 6. If you don't have everything right, the application will warn you if you have included the Activity_Create lines.
'	It will create the 'values' folder if does not exist.

'If you don't do an explicit save the project won't show up in the files
'To be committed, OR be committed As you expect; even though the changed
'File was used during the build.

' Source Code Version Control Number include: an xml file with git and build number version info, 
' Is created, made read only. It gets compiled into the R.java File 
' which makes it visible to the application after it is built.

'ISSUES TO BE AWARE OF
' The GIt Version number is that from the LAST commit.
' Therefore, be sure to 1. SAVE, 2. COMMIT, 3. COMPILE, 4 DISTRIBUTE. In that order.
' Don't make changes between steps 2 and 3: COMMIT, COMPILE. Or the compiled app will not have the exact code
' as the Git Version Hash.
' Don't version the "sCodeVerNum.xml" file.

' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub
Sub ProcessVersionInfo(Activity1 As Activity,tGitVerString As String,tHGVerString As String, tBuildNumString As String)

	' The Git Version string is embedded in the objects/res/values gitversion.xml file, which is generated NEW at each compile.
	' Examine it if the strings that show up in the toast message don't seem right.
	If tGitVerString = Null Then	' Comment out these If/End If; lines after you have the Git Version working and tested.
		Msgbox("Create Folder 'values' for XML value in objects/res/", "Error: RECOMPILE")
		Activity1.Finish
	Else
	' 'Git Version Number: Tag, Commit number since last tag, and Hash (Hash in hex preceded by a "g", all lower case hex)
		ToastMessageShow (tGitVerString &":" &tHGVerString &":" &tBuildNumString,True)	' Similar to this: V1Bat-1-ga62fb1. See TestGitVersion.B4A
		ToastMessageShow("Modify the buildnum.h file to have a starting number to suit you.", True)
	End If

End Sub
Sub GetBuildNum(inStr As String) As String

If inStr = "" Then
	Return "???"
End If

Dim sString As String = "BuildNumber: new buildnumber is"
Dim lenSrchStr, lenTstr As Int
Dim pos, len1 As Int
Dim numStr As String
Dim sf As StringFunctions

lenTstr = sf.Len(inStr)
lenSrchStr = sf.Len(sString)

pos = sf.At(inStr, sString)
len1 = lenTstr - pos
numStr = sf.Right(inStr, len1)
numStr = sf.Right(numStr, len1-lenSrchStr)	' now get the last few characters, for the number

'ToastMessageShow("Num=" & numStr, True)
Return sf.Trim(numStr)
End Sub

