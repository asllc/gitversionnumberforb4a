REM !!!! TESTING is most efficient if done while executing this batch file from a cmd window, from the objects dir/folder.

REM This is the line to put in the Project Attributes.
REM #CustomBuildAction: 1, C:\Program Files (x86)\Anywhere Software\AdditionalLibraries\sCodeVerNum.bat, dummy_arg1
REM The buildnum option depends on the exe being placed in the root project directory

REM "c:\Program Files (x86)\Git\bin\git.exe" describe --tags --long --always 1>> sCodeVerNum.xml

REM  'Git Version Number: Tag, Commit number since last tag, and Hash (Hash in hex preceded by a "g")

REM '!!! This batch file generates an xml file with gitversion_num String which you can 
REM access from any place in the program. Also, b4aBuild_num string can be used to get the build number.
REM When TESTING, examine the giversion.xml file in the values folder.
REM http://stackoverflow.com/questions/245395/hidden-features-of-windows-batch-files

SET GITPATH="%ProgramFiles(x86)%"\Git\bin\git.exe
SET GITDRV= 
REM %CD% is the Current WOrking Directory, which in B4A is the objects directory.



SET DPATH=%CD%\..\
REM be sure the dpath and xpath are one folder up from the objects, which get deleted each compile.

SET XPATH=%DPATH%buildnum.exe
SET HGXPATH=%ProgramFiles%\TortoiseHg\hg.exe

SET XMLPATHONLY=.\res\values
SET WPATH=c:\windows\system32\
SET XMLPATH=%XMLPATHONLY%\sCodeVerNum.xml
SET EMSG=!!!!!INVALID-PATH-TO-GIT-EXE!!!!!!%GITPATH%
SET XMSG=!!!!!INVALID PATH TO BUILDNUM EXE!!!!!!%XPATH%
SET HMSG=!!!!!INVALID-PATH-TO-HG-EXE!!!!!!%HGXPATH%

SET HGCMD="%HGXPATH%" parents --template '{latesttag}+{latesttagdistance}'

dir /ad %XMLPATHONLY% 1>NUL 2> NUL
if %ERRORLEVEL% EQU 0 (
    echo is a folder
) else (
 REM   echo No SUch Folder   >> %XMLPATH%
mkdir .\res\values
)

rem @echo off

If NOT Exist %%GITPATH%% GOTO NOGIT
 SET EMSG=""
:NOGIT


If NOT Exist %XPATH% GOTO NOBLD
 SET XMSG=""
:NOBLD

If NOT Exist %HGXPATH% GOTO NOHG
 SET HMSG=""
:NOHG

%WPATH%attrib.exe -r %XMLPATH%


REM c:\windows\system32\del %XMLPATH%

echo ^<?xml version="1.0" encoding="utf-8"?^>^<resources^>  > %XMLPATH%

echo    ^<string name="gitversion_num"^>   >> %XMLPATH%

%GITPATH% describe --tags --long --always 1>> %XMLPATH%

echo ^<^/string^>  >> %XMLPATH%

:: Start the Build Number String
echo    ^<string name="b4aBuild_num"^>   >> %XMLPATH%

dir %GITPATH% 1>NUL 2> NUL
if %ERRORLEVEL% EQU 0 (
    echo is a file
) else (
    echo %EMSG%   >> %XMLPATH%
)
If Exist %XPATH% GOto X2
echo    %XMSG%   >> %XMLPATH%
:X2

If Exist %HGXPATH% GOto X3
echo    %HMSG%   >> %XMLPATH%
:X3

rem dir %HGXPATH% 1>NUL 2> NUL
rem if %ERRORLEVEL% EQU 0 (
rem     echo is a file %%HGXPATH%% >> %XMLPATH%
rem ) else (
rem     echo %HMSG%   >> %XMLPATH%
rem )
rem echo    %%GITPATH%%   >> %XMLPATH%
REM echo %1% >> %XMLPATH%

SET BLDDOTH=buildnumber.h

If NOT Exist %XPATH% GOTO BLDNO
%WPATH%attrib.exe -r %DPATH%%BLDDOTH%
"%XPATH%" %DPATH% 1>> %XMLPATH%
%WPATH%attrib.exe +r %DPATH%%BLDDOTH%
:BLDNO



REM Buildnum.h is located in the root folder of the project.
Rem echo 00  >> %XMLPATH%
REM this line with the zero, zero build has to be replace with a routine like git.exe describe tags to put out a valid build number(Buildnum.exe)

echo ^<^/string^>  >> %XMLPATH%	
	:: end the build number string


 :: Start the Mercurial String
echo    ^<string name="b4aHG_num"^>   >> %XMLPATH%

rem  %HGCMD% 1>> %XMLPATH%
echo Mercurial Number Not Implemented. 1>> %XMLPATH%
echo ^<^/string^>  >> %XMLPATH%	
 	:: end the Mercurial string


echo ^<^/resources^>  >> %XMLPATH%
%WPATH%attrib.exe +r %XMLPATH%

REM !!! It may be possible to trap the stderr, but this has not been done yet.